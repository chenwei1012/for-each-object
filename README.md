# 一个Weixin Script (wxs) 对象迭代器

> 使用示例：
``` shell
var feo = require('forEachObj.wxs');

var obj = {
  // ...something object
}

feo.forEachObj(obj, function(key, value) {
  // ...something code
});
```
具体使用请详见项目源码。
